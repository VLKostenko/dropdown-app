import React from 'react';

import './App.scss';
import repeat from "./utils/helpers";
import Autocomplete from "./components/Autocomplete/Autocomplete";

function App() {

  const arr = [];

  repeat(5000, i => {
    arr.push(`Unit ${i + 1}`);
  });

  return (
    <div className="App">
      <Autocomplete options={arr}/>
    </div>
  );
}

export default App;
