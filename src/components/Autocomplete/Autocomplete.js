import React, { useState } from "react";

function Autocomplete({ options }) {
  const propsOptions = options;
  const [activeOption, setActiveOption] = useState(0);
  const [filteredOptions, setFilteredOptions] = useState([]);
  const [userInput, setUserInput] = useState('');

  let optionList;

  const onClick = (e) => {
    setUserInput(e.currentTarget.innerText)
  };

  const onChange = (e) => {
    const options = propsOptions;
    const userInput = e.currentTarget.value;

    const filteredOptions = options.filter(
      optionName =>
        optionName.toLowerCase().indexOf(userInput.toLowerCase()) > -1
    );

    setActiveOption(0)
    setFilteredOptions(filteredOptions)
    setUserInput(e.currentTarget.value);
  };

  const onKeyDown = (e) => {

    if (e.keyCode === 13) {
      setActiveOption(0)
      setUserInput(filteredOptions[activeOption])

    } else if (e.keyCode === 38) {
      if (activeOption === 0) {
        return;
      }
      setActiveOption(activeOption - 1)
    } else if (e.keyCode === 40) {
      if (activeOption === filteredOptions.length - 1) {
        return;
      }
      setActiveOption(activeOption + 1)
    }
  };

  if(userInput) {
    if (filteredOptions.length) {
      optionList = (
        <ul className="options">
          {filteredOptions.map((optionName, index) => {
            return (
              <li key={index} onClick={onClick}>
                {optionName}
              </li>
            );
          })}
        </ul>
      );
    }
  } else {
    optionList = (
      <div className="no-options">
        <p>Search "Unit"</p>
      </div>
    );
  }

  return (
    <React.Fragment>
      <div className="search">
        <input
          type="text"
          placeholder="search"
          className="search-box"
          onChange={onChange}
          onKeyDown={onKeyDown}
          value={userInput}
        />
      </div>
      {optionList}
    </React.Fragment>
  )
}

export default Autocomplete
