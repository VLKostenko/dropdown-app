import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Autocomplete from './Autocomplete';

test('component should render', () => {
    const { getByText } = render(<Autocomplete />);
});

test('renders search unit', () => {
    const { getByText } = render(<Autocomplete />);
    const linkElement = getByText(/Search "Unit"/i);
    expect(linkElement).toBeInTheDocument();

});

test('can change the value of an input', () => {
    const { getByTestId } = render(<input data-testid='input'/>);
    
    const element = getByTestId('input');
    
    fireEvent.change(element, { target: { value: 'desired text' } });
    
    expect(element).toHaveValue('desired text');
});