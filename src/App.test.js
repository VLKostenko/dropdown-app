import React from 'react';
import { render } from '@testing-library/react';
import App from './App';
import Autocomplete from './components/Autocomplete/Autocomplete';

test('component should render', () => {
  const { getByText } = render(<App />);
});

test('component should render child', () => {
  const { getByText } = render(<Autocomplete />);
});
